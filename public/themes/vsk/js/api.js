function getHomeById(id) {
    return fetch('/api/homes/' + id).then(resp => resp.json());
}

function getHomeDatePriceById(id, date) {
    return fetch('/api/homes/' + id + '/price?date=' + date).then(resp => resp.json());
}
