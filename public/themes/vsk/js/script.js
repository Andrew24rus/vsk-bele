let fpickr = flatpickr(".date-picker", {
    locale: "ru",
    altInput: true,
    altFormat: 'd.m.Y',
    allowInput: true,
    disableMobile: true,
    minDate: "today"
});

$('[data-target="#modal-reservation"]').click(e => {
    let el = $(e.currentTarget);
    let home_id = el.data('home-id');

    modal.fetchHomeById(home_id).then(() => {
        if (el.data('date')) {
            modal.from = el.data('date');
            //modal.to = el.data('date');
        }
    });
});

$('#mobile-menu-button').click(() => $('#mobile-menu').addClass('show'));
$('#mobile-menu').click((e) => $(e.currentTarget).removeClass('show'));
$('#mobile-menu-close').click((e) => $('#mobile-menu').removeClass('show'));


/* Галлерея */
let swiper;

$('[data-swiper-id] img').click(function (e) {
    let img = $(e.currentTarget);
    let index = img.data('image-index');
    let swiper_id = '#' + img.closest('[data-swiper-id]').data('swiper-id');

    $(swiper_id).addClass('show');

    swiper = new Swiper (swiper_id + ' .swiper-container', {
        initialSlide: index,
        autoplay: false,
        preloadImages: false,
        lazy: true,
        keyboard: true,
        centeredSlides: true,
        pagination: {
            el: '.swiper-pagination',
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        }
    });
});

function closeSwiper(target) {
    target.removeClass('show');

    if (swiper instanceof Swiper) {
        swiper.destroy();
    }
}

$('.swiper-button-close').click(function (e) {
    closeSwiper($(e.target).closest('.swiper'));
});
/*************************************************/

/* Range Component */
var sheet = document.createElement('style'),
    $rangeInput = $('.range input');

document.body.appendChild(sheet);

var getTrackStyle = function (el) {
    var curVal = el.value,
        style = '';

    if ($(el).data('any-val')) {
        curVal = Number(curVal) + 1;
    }

    // Set active label
    $('.range-labels li').removeClass('active selected');

    var curLabel = $('.range-labels').find('li:nth-child(' + curVal + ')');

    curLabel.addClass('active');

    return style;
}

$rangeInput.on('input', function () {
    sheet.textContent = getTrackStyle(this);
});

// Change input value on label click
$('.range-labels li').on('click', function () {
    var index = $(this).index();

    $rangeInput.val(index).trigger('input');
});
