<?php

namespace App\Helpers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Slim\Handlers\PhpError;

class ErrorExceptions
{
    private $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function __invoke($request, $response, $exception)
    {
        if ($exception instanceof ModelNotFoundException) {
            return $this->container->view->render($response->withStatus(404), 'public/404.twig');
        }

        // Заглушка 500 ошибки для production версии
        if (!in_array($_SERVER['REMOTE_ADDR'], ['127.0.0.1', '::1'])) {
            return $this->container->view->render($response->withStatus(500), 'public/500.twig');
        }

        $error = new PhpError(true);

        return $error($request, $response, $exception);
    }
}
