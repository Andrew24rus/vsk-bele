<?php


namespace App\Model;

use Illuminate\Database\Eloquent\Model as Eloquent;

class VSKSettings extends Eloquent
{
    protected $table = 'vsk_settings';

    public $timestamps = false;

    protected $fillable = ['prop', 'value'];
}
