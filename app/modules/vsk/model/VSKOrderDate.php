<?php

namespace ADFM\Model;

use Illuminate\Database\Eloquent\Model as Eloquent;

class VSKOrderDate extends Eloquent
{
    protected $table = 'vsk_order_dates';
    protected $guarded = [];
    public $timestamps = false;
}
