<?php


namespace App\Model;

use Illuminate\Database\Eloquent\Model as Eloquent;

class VSKTempToken extends Eloquent
{
    protected $table = 'vsk_temp_tokens';

    protected $guarded = [];

    public $timestamps = true;
}
