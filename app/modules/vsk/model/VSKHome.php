<?php

namespace ADFM\Model;

use ADFM\Helper\FilesTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class VSKHome extends Eloquent
{
    use SoftDeletes;
    use FilesTrait;

    protected $table = 'vsk_homes';
    protected $guarded = [];
    public $timestamps = true;

    protected $casts = [
        'electric' => 'boolean',
        'shower' => 'boolean',
        'bestseller' => 'boolean'
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        Relation::morphMap([
            'vsk-home' => get_class($this),
        ]);
    }

    public function photos()
    {
        return $this->morphMany(File::class, 'entity')->orderBy('position');
    }

    public function priceList()
    {
        return $this->belongsToMany(
            VSKDate::class,
            'vsk_date_home',
            'vsk_home_id',
            'vsk_date_id'
        )->withPivot(['weekdays_price', 'weekend_price', 'wholesale']);
    }

    public function orders()
    {
        return $this->hasMany(VSKOrder::class, 'vsk_home_id', 'id');
    }

    public function scopeOrdersOn($query, Carbon $date)
    {
        return $query->with(['orders' => function ($q) use ($date) {
            $q->whereHas('dates', function ($q) use ($date) {
                $q->where('date', $date->format('Y-m-d'));
            })->reserved();
        }]);
    }

    public function scopeReservedOn($query, Carbon $date)
    {
        return $query->whereHas('orders', function ($q) use ($date) {
            $q->whereHas('dates', function ($q) use ($date) {
                $q->where('date', $date->format('Y-m-d'));
            })->reserved();
        });
    }

    public function scopeNotReservedOn($query, Carbon $date)
    {
        return $query->whereDoesntHave('orders', function ($q) use ($date) {
            $q->whereHas('dates', function ($q) use ($date) {
                $q->where('date', $date->format('Y-m-d'));
            })->reserved();
        });
    }

    public function scopePriceListOn($query, Carbon $date)
    {
        return $query->with(['priceList' => function($q) use($date) {
            $q->where('from', $date->copy()->startOfWeek());
        }]);
    }

    protected static function boot()
    {
        parent::boot();

        static::saving(function($home) {
            $home->slug = Str::slug($home->title);
            $home->percentage = $home->percentage > 100 ? 100 : $home->percentage < 0 ? 0 : $home->percentage;
        });

        static::created(function($home) {
            $dates_id = VSKDate::all()->pluck('id');
            $home->priceList()->sync($dates_id);
        });
    }
}
