<?php

namespace ADFM\Model;

use ADFM\Helper\FilesTrait;
use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class VSKService extends Eloquent
{
    use SoftDeletes;
    use FilesTrait;

    protected $table = 'vsk_services';
    protected $guarded = [];
    public $timestamps = true;
    protected $appends = ['url'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        Relation::morphMap([
            'vsk-service' => get_class($this),
        ]);
    }

    public function photos()
    {
        return $this->morphMany(File::class, 'entity')->orderBy('position');
    }

    protected static function boot()
    {
        parent::boot();

        static::saving(function($service) {
            $service->slug = Str::slug($service->title);
        });
    }
}
