<?php

namespace ADFM\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

class VSKOrder extends Eloquent
{
    use SoftDeletes;

    protected $table = 'vsk_orders';
    protected $guarded = [];
    public $timestamps = true;
    public $appends = ['status_text'];

    public static $weekendDays = ['Fri', 'Sat'];

    public function getStatusTextAttribute($order)
    {
        switch ($this->status) {
            case 'payed_card':
                return 'Оплачено картой';
            case 'payed_money':
                return 'Оплачено наличкой';
            case 'manual_order':
                return 'Забронировано без оплаты';
            default:
                return 'Статус не задан \ ошибка';
        }
    }

    public function setPhoneAttribute($phone)
    {
        $this->attributes['phone'] = preg_replace('/\D/', '', $phone);
    }

    public function home()
    {
        return $this->hasOne(VSKHome::class, 'id', 'vsk_home_id');
    }

    public function dates()
    {
        return $this
            ->hasMany(VSKOrderDate::class, 'vsk_order_id', 'id')
            ->orderBy('date', 'asc');
    }

    public function scopeReserved($q)
    {
        return $q->whereIn('status', ['payed_card', 'payed_money', 'manual_order']);
    }

    protected static function boot()
    {
        parent::boot();

        static::saving(function($order) {
            $order->phone = preg_replace('/\D/', '', $order->phone);

            //Проверяем, если домик уже занят
            $existed = VSKOrder::whereHas('dates', function($q) use($order) {
                $q->where('date', $order->date);
            })
                ->where('vsk_home_id', $order->vsk_home_id)
                ->reserved();

            if ($order->id) {
                $existed->where('id', '!=', $order->id);
            }

            $existed = $existed->first();

            if (in_array($order['status'], ['payed_card', 'payed_money', 'manual_order']) && $existed) {
                throw new \Exception('Домик уже забронирован');
            }
        });
    }
}
