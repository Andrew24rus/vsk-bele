<?php

namespace ADFM\Model;
use Illuminate\Database\Eloquent\Model as Eloquent;

class VSKDate extends Eloquent
{
    protected $table = 'vsk_dates';
    protected $guarded = [];
    public $timestamps = false;

    public function homes()
    {
        return $this->belongsToMany(
            VSKHome::class,
            'vsk_date_home',
            'vsk_date_id',
            'vsk_home_id'
        )
            ->withPivot(['weekdays_price', 'weekend_price', 'wholesale'])
            ->orderBy('position');
    }
}
