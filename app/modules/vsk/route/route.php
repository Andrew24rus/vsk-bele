<?php

use App\Model\VSKTempToken;
use Carbon\Carbon;

$app = $this->app;
$container = $app->getContainer();

$app->get('/homes', '\ADFM\Controller\VSKHomeController:showHomes')->setName('vsk.homes.show');
$app->get('/homes/{slug}', '\ADFM\Controller\VSKHomeController:showHomeBySlug')->setName('vsk.homes.showBySlug');

$app->get('/services', '\ADFM\Controller\VSKServiceController:showServices')->setName('vsk.services.show');
$app->get('/services/{slug}', '\ADFM\Controller\VSKServiceController:showServiceBySlug')->setName('vsk.services.showBySlug');
$app->get('/reservation', '\ADFM\Controller\VSKController:showReservation')->setName('vsk.reservation.show');

$app->get('/ordering-success', '\ADFM\Controller\VSKOrderController:showSuccess')->setName('vsk.orders.successReservation');
$app->get('/ordering-fail', '\ADFM\Controller\VSKOrderController:showFail')->setName('vsk.orders.failReservation');
$app->post('/orders', '\ADFM\Controller\VSKOrderController:createPublicOrder')->setName('vsk.orders.create');

$app->get('/api/homes/{id}', '\ADFM\Controller\VSKHomeController:getHomeById')->setName('vsk.homes.apiGetById');
$app->get('/api/homes/{id}/price', '\ADFM\Controller\VSKHomeController:getPriceByRange')->setName('vsk.homes.apiGetPrice');

$app->group('/admin/vsk', function() use($app) {
    $app->get('', '\ADFM\Controller\VSKOrderController:showAdminMain')->setName('vsk.main.get');

    $app->get('/price', '\ADFM\Controller\VSKController:showMain')->setName('vsk.dates.get');

    $app->get('/homes', '\ADFM\Controller\VSKHomeController:showAdminHomes')->setName('vsk.homes.get');
    $app->get('/homes/{id:new|[0-9]+}', '\ADFM\Controller\VSKHomeController:showAdminHome')->setName('vsk.homes.getById');

    $app->get('/orders', '\ADFM\Controller\VSKOrderController:showAdminOrders')->setName('vsk.orders.get');
    //$app->get('/orders/{id:new|[0-9]+}', '\ADFM\Controller\VSKOrderController:showAdminOrder')->setName('vsk.orders.getById');
    $app->get('/orders/{month}/{year}', '\ADFM\Controller\VSKOrderController:showAdminOrdersByMonth')->setName('vsk.orders.getByMonth');

    $app->get('/services', '\ADFM\Controller\VSKServiceController:showAdminServices')->setName('vsk.services.get');
    $app->get('/services/{id:new|[0-9]+}', '\ADFM\Controller\VSKServiceController:showAdminService')->setName('vsk.services.getById');
})->add(new Registred($container));



$app->group('/api/vsk', function() use($app) {
    $app->post('/dates', '\ADFM\Controller\VSKController:savePriceList')->setName('vsk.price.set');

    $app->post('/homes/percentage', '\ADFM\Controller\VSKHomeController:setPercentage')->setName('vsk.homes.setPercentage');
    $app->post('/homes[/{id}]', '\ADFM\Controller\VSKHomeController:createOrUpdateHome')->setName('vsk.homes.set');
    $app->delete('/homes/{id}', '\ADFM\Controller\VSKHomeController:softDeleteHome')->setName('vsk.homes.delete');

    //$app->post('/orders[/{id}]', '\ADFM\Controller\VSKOrderController:createOrUpdateOrder')->setName('vsk.orders.set');
//    $app->delete('/orders/{id}', '\ADFM\Controller\VSKOrderController:softDeleteOrder')->setName('vsk.orders.delete');
    $app->get('/orders/{id}/delete', '\ADFM\Controller\VSKOrderController:softDeleteOrder')->setName('vsk.orders.delete');

    $app->post('/services[/{id}]', '\ADFM\Controller\VSKServiceController:createOrUpdateService')->setName('vsk.services.set');
    $app->delete('/services/{id}', '\ADFM\Controller\VSKServiceController:softDeleteService')->setName('vsk.services.delete');

    $app->post('/settings/disable-reservation', '\ADFM\Controller\VSKController:disableReservation')->setName('vsk.settings.disableReservation');
    $app->post('/settings/enable-reservation', '\ADFM\Controller\VSKController:enableReservation')->setName('vsk.settings.enableReservation');
})->add(new Registred($container));

$app->group('/api/vsk', function () use($app) {
    $app->post('/{token}/getOrderingUrl', '\ADFM\Controller\VSKController:getOrderingUrl');
});

$app->group('', function () use ($app) {
    $app->group('/admin/vsk', function () use ($app) {
        $app->get('/orders/{id:new|[0-9]+}', '\ADFM\Controller\VSKOrderController:showAdminOrder')
            ->setName('vsk.orders.getById');
    });

    $app->group('/api/vsk', function () use ($app) {
        $app->post('/orders[/{id}]', '\ADFM\Controller\VSKOrderController:createOrUpdateOrder')
            ->setName('vsk.orders.set');
    });
})->add(function ($request, $response, $next) use ($container) {
    /* Проверка токена */
    $validateToken = function ($token) {
        $temp = VSKTempToken::where('token', $token)
            ->where('expires_in', '>', Carbon::now())
            ->first();

        return !!$temp;
    };

    $params = $request->getParams();

    if (isset($params['token']) && $validateToken($params['token'])) {
        return $response = $next($request, $response);
    }

    $registred = new Registred($container);

    return $registred($request, $response, $next);
});
