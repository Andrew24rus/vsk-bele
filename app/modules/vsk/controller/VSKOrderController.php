<?php


namespace ADFM\Controller;


use ADFM\Helper\Config;
use ADFM\Model\VSKHome;
use ADFM\Model\VSKDate;
use ADFM\Model\VSKOrder;
use App\Model\VSKTempToken;
use App\Modules\VSK\Builders\OrderBuilder;
use App\Modules\VSK\Builders\PriceBuilder;
use App\Modules\VSK\Builders\SberbankBuilder;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Support\Arr;
use Nette\Mail\Message;
use Nette\Mail\SmtpException;
use Nette\Mail\SmtpMailer;
use Slim\Http\Request;

class VSKOrderController extends Controller
{
    private $config;

    public function __construct($container)
    {
        parent::__construct($container);

        $this->config = \Spyc::YAMLLoad(__DIR__ . '/../config.yaml');
    }

    public function showAdminOrders($request, $response, $args)
    {
        $this->twig_vars['orders'] = VSKOrder::with(['home', 'dates'])
            ->orderBy('id', 'desc')
            ->get();
        $this->view = 'orders-admin.twig';
        $this->render();
    }

    public function showAdminMain($request, $response, $args)
    {
        $m = date('m');

        $args = [
            'month' => $m >= 6 && $m <= 8 ? date('F') : 'june',
            'year' => date('Y')
        ];

        $this->showAdminOrdersByMonth($request, $response, $args);
    }

    public static function getMonthRange($month, $year)
    {
        $months = [
            'january', 'february', 'march', 'april', 'may', 'june',
            'july', 'august', 'september', 'october', 'november', 'december'
        ];

        $month = mb_strtolower($month);

        if (in_array($month, $months)) {
            $month = array_search($month, $months) + 1;
        } else {
            $month = date('m');
        }

        $from = "$year-$month-01";
        $to = "$year-$month-31";

        return [$from, $to];
    }

    public static function getScheduleByDateRange($from, $to)
    {
        $from = Carbon::parse($from)->startOfWeek()->format('Y-m-d');

        return VSKDate::whereBetween('from', [$from, $to])
            ->with('homes')
            ->get();
    }

    public static function getHomeOrdersByDateRange($from, $to)
    {
        return VSKHome::with(['orders' => function($q) use ($from, $to) {
            $q->reserved()->with(['dates' => function($q) use ($from, $to) {
                $q->whereBetween('date', [$from, $to]);
            }]);
        }])
            ->orderBy('position')
            ->orderBy('title')
            ->get();
    }

    public function showAdminOrdersByMonth($request, $response, $args)
    {
        $range = self::getMonthRange($args['month'], $args['year']);
        $from = $range[0];
        $to = $range[1];
        $this->twig_vars['month'] = $from;
        $this->twig_vars['price_list'] = self::getScheduleByDateRange($from, $to);
        $this->twig_vars['homes'] = self::getHomeOrdersByDateRange($from, $to);

        $this->view = 'main-admin.twig';
        $this->render();
    }

    public function showAdminOrder($request, $response, $args)
    {
        $params = $request->getParams();
        $home = VSKHome::orderBy('position')->get();
        $order = isset($args['id']) ? VSKOrder::with('dates')->find($args['id']) : [];
        if ($order) {
            if (isset($params['token'])) {
                VSKTempToken::where('token', $params['token'])->delete();
            }
        } else {
            if (isset($params['home'])) {
                $order['vsk_home_id'] = $params['home'];
            }

            if (isset($params['date'])) {
                $order['from'] = $params['date'];
                $order['to'] = date('Y-m-d', strtotime($params['date'] . ' +1 day'));
            }
        }

        if (isset($params['token']) && $params['token']) {
            $this->twig_vars['token'] = $params['token'];
        }

        $this->twig_vars['order'] = $order;
        $this->twig_vars['homes'] = $home;
        $this->view = 'order-admin.twig';
        $this->render();
    }

    public function createOrUpdateOrder($request, $response, $args)
    {
        $params = $request->getParams();
        $orderBuilder = new OrderBuilder();
        $order = null;

        try {
            $priceBuilder = (new PriceBuilder())
                ->setHomeById($params['home_id'])
                ->setRange($params['from'], $params['to']);

            if (isset($args['id'])) {
                $order = VSKOrder::find($args['id']);
                $priceBuilder->setCurrentOrder($order);
                $orderBuilder->setOrder($order);
            }

            $orderBuilder
                ->setHomeById($params['home_id'])
                ->setAmount($priceBuilder->getPrice())
                ->setDates($priceBuilder->getRange());

            foreach ($params as $key => $value) {
                switch ($key) {
                    case 'status':
                        $orderBuilder->setProperty('status', $value);
                        break;
                    case 'name':
                        $orderBuilder->setClientName($value);
                        break;
                    case 'phone':
                        $orderBuilder->setClientPhone($value);
                        break;
                    case 'note':
                        $orderBuilder->setNote($value);
                        break;
                    default:
                        break;
                }
            }

            $order = $orderBuilder->save();
            if(isset($args['id'])) {
                $this->ci['flash']->addMessage('success', 'Изменения сохранены');
            } else {
                $this->ci['flash']->addMessage('success', 'Заявка оформлена');
            }

        } catch (\Exception $e) {
            $this->ci['flash']->addMessage('error', $e->getMessage());
        }

        $p = [];

        if ($order) {
            $link = $order->id;
        } else {
            $link = 'new';
            $p = [
                'home' => $params['home_id'],
                'from' => $params['from'],
                'to' => $params['to']
            ];
        }

        if (isset($params['token']) && $params['token']) {
            $p['token'] = $params['token'];
        }

        return $response->withHeader('Location', '/admin/vsk/orders/' . $link . '?' . http_build_query($p));
    }

    public function softDeleteOrder($request, $response, $args)
    {
        VSKOrder::destroy($args['id']);

        return $response->withHeader('Location', '/admin/vsk/orders');
    }

    public function createPublicOrder($request, $response, $args)
    {
        try {
            $home_id = self::getHomeIdParam($request);
            $from = self::getFromParam($request);
            $to = self::getToParam($request);
            $client_name = self::getClientNameParam($request);
            $client_phone = self::getClientPhoneParam($request);
            $client_email = self::getClientEmailParam($request);
            $home = VSKHome::findOrFail($home_id);

            $priceBuilder = (new PriceBuilder())
                ->setHome($home)
                ->setRange($from, $to, true);

            $price = $priceBuilder->getPrice();
            $amount = $home->percentage ? ($price * ($home->percentage / 100)) : $price;

            $amount = round($amount, 2);

            $orderBuilder = (new OrderBuilder())
                ->setHome($home)
                ->setClientName($client_name)
                ->setClientPhone($client_phone)
                ->setClientEmail($client_email)
                ->statusPrepare();

            $period = $priceBuilder->getRange();
            $period = CarbonPeriod::create($period[0], array_slice($period, -1)[0]);

            $order = $orderBuilder
                ->setDates($period->toArray())
                ->setAmount($amount)
                ->save();

            $orderNumber = substr('00000' . $order->id, -6);

            $baseUrl = $request->getUri()->getBaseUrl();
            $sberbank = new SberbankBuilder();
            $data = $sberbank
                ->setCredentials('vskbele-api', 'vskbele')
                ->setOrderNumber($orderNumber)
                ->setAmount($amount * 100)
                ->setReturnUrl($baseUrl . '/ordering-success')
                ->setFailUrl($baseUrl . '/ordering-fail')
                ->setDescription("Бронирование {$home->title}")
                ->register();

            if (isset($data->errorCode)) {
                throw new \Exception($data->errorMessage, $data->errorCode);
            }

            $orderBuilder
                ->setProperty('deleted_at', '0')
                ->setSberbankId($data->orderId)
                ->save();

            return $response->withRedirect($data->formUrl);

        } catch (\Exception $e) {
            exit($e->getMessage());
            return $response->withRedirect('/');
        }
    }

    public function showSuccess($request, $response, $args)
    {
        $params = $request->getParams();

        if (self::getParam($request, 'orderId')) {
            $orderId = $params['orderId'];
            $order = VSKOrder::withTrashed()->where('sberbank_id', $orderId)
                ->with('home', 'dates')
                ->orderBy('date', 'asc')
                ->first();
            $order->restore();
            if ($order) {
                $order->update(['status' => 'payed']);

                if ($order->email) {
                    $this->sendReceipt($order);
                }

                $this->sendTelegram($order);
                $this->sendEmail($order);

                $this->twig_vars['order'] = $order;
                $this->view = 'ordering-success.html.twig';
                $this->render();

                return true;
            }
        }

        return $response->withRedirect('/');
    }

    private function sendReceipt(VSKOrder $order)
    {
        $receipt = $this->ci->view->fetch('email/receipt.html.twig', ['order' => $order]);
        $config = Config::getInstance()['mail'];

        $mail = new Message;
        $mail->setFrom("Беле ВСК <{$config['username']}>")
            ->addTo($order->email)
            ->setSubject('Кассовый чек от "Беле ВСК" ' . $order->created_at->format('d.m.Y H:i'))
            ->setHtmlBody($receipt);

        try {
            $mailer = new SmtpMailer($config);
            $mailer->send($mail);
        } catch (SmtpException $e) {
        }
    }

    private function sendTelegram(VSKOrder $order)
    {
        $order_number = substr('00000' . $order->id, -6);
        $bot_id = $this->config['vsk_bot_id'];
        $chat_ids = $this->config['telegram_chat_id'];
        $parse_mode = 'Markdown';
        $days = $order->dates->map(function ($d) {
            return Carbon::createFromFormat('Y-m-d', $d->date);
        })->toArray();
        $debt = (($order->amount / $order->home->percentage) * 100) - $order->amount;

        $text = "*Беле ВСК: Информация о заказе №{$order_number}*\n" .
                "*Дом:* [{$order->home->title}]({$_SERVER['REQUEST_SCHEME']}://{$_SERVER['HTTP_HOST']}/homes/{$order->home->id})\n" .
                "*Процент бронирования:* {$order->home->percentage}%\n" .
                "*Имя клиента:* {$order->name}\n" .
                "*Номер телефона:* +{$order->phone}\n" .
                "*Email:* {$order->email}\n" .
                "*Оплачено:* {$order->amount} руб.\n" .
                "*Долг:* " . round($debt, 2) . " руб.\n".
                "*Дата заезда:* {$days[0]->format('d.m.Y')}\n" .
                "*Дата выезда:* " . end($days)->addDay()->format('d.m.Y') . "\n" .
                "*Дней проживания:* " . $order->dates->count();

        $text = urlencode($text);

        foreach ($chat_ids as $chat_id) {
            $url = "https://api.telegram.org/bot{$bot_id}/sendMessage?chat_id={$chat_id}&text=${text}&parse_mode={$parse_mode}";

            file_get_contents($url);
        }
    }

    private function sendEmail(VSKOrder $order)
    {
        $template = $this->ci->view->fetch('email/order.html.twig', ['order' => $order]);
        $config = Config::getInstance()['mail'];

        $mail = new Message;
        $mail->setFrom("Беле ВСК <{$config['username']}>")
            ->addTo('web@wtolk.ru')
            ->addTo('Info@vskbele.ru')
            ->setSubject('Беле ВСК: Заказ №' . substr('00000' . $order->id, -6))
            ->setHtmlBody($template);

        try {
            $mailer = new SmtpMailer($config);
            $mailer->send($mail);
        } catch (SmtpException $e) {
        }
    }

    public function showFail($request, $response, $args)
    {
        $params = $request->getParams();

        if (self::getParam($request, 'orderId')) {
            $orderId = $params['orderId'];
            $order = VSKOrder::where('sberbank_id', $orderId)->with('home')->first();

            if ($order) {
                $order->update(['status' => 'canceled']);

                $this->twig_vars['order'] = $order;
                $this->view = 'ordering-fail.html.twig';
                $this->render();

                return true;
            }
        }

        return $response->withRedirect('/');
    }

    private static function getParam(Request $request, $key)
    {
        $params = $request->getParams();

        if (Arr::exists($params, $key) && $params[$key]) {
            return $params[$key];
        }

        return false;
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \Exception
     */
    private static function getHomeIdParam(Request $request)
    {
        if ($home_id = self::getParam($request, 'home_id')) {
            return $home_id;
        }

        throw new \Exception('Домик не выбран');
    }

    /**
     * @param Request $request
     * @return Carbon
     * @throws \Exception
     */
    private static function getFromParam(Request $request)
    {
        if ($date = self::getParam($request, 'from')) {
            return $date;
        }

        throw new \Exception('Поле "Дата" не заполнено');
    }

    private static function getToParam(Request $request)
    {
        if ($date = self::getParam($request, 'to')) {
            return $date;
        }

        throw new \Exception('Поле "Дата" не заполнено');
    }

    /**
     * @param Request $request
     * @return string
     * @throws \Exception
     */
    private static function getClientNameParam(Request $request)
    {
        if ($name = self::getParam($request, 'name')) {
            return $name;
        }

        throw new \Exception('Поле "Имя клиента" не заполнено');
    }

    private static function getClientPhoneParam(Request $request)
    {
        if ($phone = self::getParam($request, 'phone')) {
            return $phone;
        }

        throw new \Exception('Поле "Телефон" не заполнено');
    }

    private static function getClientEmailParam(Request $request)
    {
        if ($email = self::getParam($request, 'email')) {
            return $email;
        }

        return null;
    }

    /**
     * @param Carbon $date
     * @return bool
     */
    private static function isWeekend(Carbon $date)
    {
        return $date->isFriday() || $date->isSaturday();
    }

    /**
     * @param VSKHome $home
     * @return mixed
     * @throws \Exception
     */
    private static function getPriceListHome(VSKHome $home)
    {
        if ($priceList = $home->priceList->first()) {
            return $priceList;
        }

        throw new \Exception('Прайс-лист не найден');
    }

    /**
     * @param VSKHome $home
     * @param Carbon $date
     * @return array
     * @throws \Exception
     */
    private static function getAmount(VSKHome $home, Carbon $from, Carbon $to)
    {
        $amount = 0;
        $priceBuilder = new PriceBuilder();
        $price = $priceBuilder
            ->setHome($home)
            ->setDateRange($from, $to)
            ->getPrice();

        if ($home->percentage !== null) {
            $amount = $price * ($home->percentage / 100);
        }

        return [
            'price' => $price,
            'amount' => $amount
        ];
    }
}
