<?php

namespace ADFM\Controller;

use ADFM\Model\VSKService;

class VSKServiceController extends Controller
{
    public function showAdminServices($request, $response, $args) {
        $this->view = 'services-admin.twig';
        $this->twig_vars['services'] = VSKService::with('photos')->get();
        $this->render();
    }

    public function showAdminService($request, $response, $args) {
        $this->view = 'service-admin.twig';
        $this->twig_vars['service'] = VSKService::with('photos')->find($args['id']);
        $this->render();
    }

    public function showServices($request, $response, $args)
    {
        $this->twig_vars['services'] = VSKService::with('photos')
            ->orderBy('id', 'desc')
            ->paginate(10);
        $this->view = 'services.html.twig';
        $this->render();
    }

    public function showServiceBySlug($request, $response, $args)
    {
        $service = VSKService::with('photos')
            ->where('slug', $args['slug'])
            ->firstOrFail();
        $this->twig_vars['service'] = $service;
        $this->view = 'service.html.twig';
        $this->render();
    }

    public function createOrUpdateService($request, $response, $args)
    {
        set_time_limit(0);

        $params = $request->getParams();
        $uploader = null;

        if (isset($params['uploader'])) {
            $uploader = $params['uploader'];
            unset($params['uploader']);
        }

        if (isset($args['id'])) {
            $service = VSKService::find($args['id']);
            $service->update($params);
        } else {
            $service = VSKService::create($params);
        }

        if ($uploader) {
            $service->checkFiles('photos', $uploader);
        }

        return $response->withHeader('Location', '/admin/vsk/services/' . $service->id);
    }

    public function softDeleteService($request, $response, $args)
    {
        VSKService::destroy($args['id']);

        return $response->withHeader('Location', '/admin/vsk/services');
    }
}
