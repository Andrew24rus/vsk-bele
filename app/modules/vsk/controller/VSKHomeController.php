<?php

namespace ADFM\Controller;

use ADFM\Model\VSKHome;
use ADFM\Model\VSKOrder;
use App\Model\VSKSettings;
use App\Modules\VSK\Builders\PriceBuilder;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Carbon\Exceptions\InvalidFormatException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class VSKHomeController extends Controller
{
    public function getPriceByRange($request, $response, $args)
    {
        $params = $request->getParams();
        $result = [
            'price' => 0,
            'notifications' => []
        ];

        if (isset($params['from']) && isset($params['to'])) {
            $priceBuilder = (new PriceBuilder())
                ->setHomeById($args['id'])
                ->setRange($params['from'], $params['to'], true);

            try {
                $result['price'] = $priceBuilder->getPrice();
                $range = $priceBuilder->getRange();

                $result['from'] = $range[0]->format('Y-m-d');
                $result['to'] = array_slice($range, -1)[0]->addDays(1)->format('Y-m-d');
                $wholesale_dates = $priceBuilder->getWholesaleDates();

                foreach ($wholesale_dates as $range) {
                    $result['notification'][] = self::getWholesaleMessage($range);
                }

            } catch (InvalidFormatException | ModelNotFoundException $e) {
                $result['error'] = 'Ошибка запроса';
                $result['reserved'] = true;
            } catch (\Exception $e) {
                $result['error'] = $e->getMessage();
                $result['reserved'] = true;
            }
        } else {
            $result['reserved'] = true;
        }

        return $response->withJson($result);
    }

    private static function getWholesaleMessage(array $dates)
    {
        $from = $dates[0]->copy()->format('d.m.Y');
        $to = array_slice($dates, -1)[0]->copy()->format('d.m.Y');
        // $msg = "Бронирование домика на период c {$from} по {$to} осуществляется оптом.";
        $msg = "В определенные даты выходные дни могут быть не разбиваемы посуточно и сдаваться только совместно";

        return $msg;
    }

    public function getHomeById($request, $response, $args)
    {
        $home = VSKHome::find($args['id']);

        return $response->withJson($home);
    }

    public function showHomes($request, $response, $args)
    {
        $params = $request->getParams();

        $homes = VSKHome::with('photos', 'priceList')->orderBy('position');

        if (
            isset($params['from']) && $params['from'] &&
            isset($params['to']) && $params['to']
        ) {
            $homes->whereDoesntHave('orders', function ($q) use ($params) {
                $q->whereHas('dates', function($q) use($params) {
                    $q->where('date', '>=', $params['from'])
                        ->where('date', '<', $params['to']);
                })->reserved();
            });
        }

        if (isset($params['beds']) && $params['beds'] > 0) {
            $homes->where('beds', '>=', $params['beds']);
        }

        if (isset($params['electric'])) {
            $homes->where('electric', true);
        }

        if (isset($params['shower'])) {
            $homes->where('shower', true);
        }

        $this->twig_vars['homes'] = $homes->paginate(10);
        $this->twig_vars['params'] = $params;
        $this->view = 'vsk-homes.html.twig';
        $this->render();
    }

    public function showHomeBySlug($request, $response, $args)
    {
        $this->twig_vars['home'] = VSKHome::where('id', $args['slug'])->with('photos')->firstOrFail();
        $this->view = 'vsk-home.html.twig';
        $this->render();
    }

    public function showAdminHomes($request, $response, $args) {
        $this->view = 'homes-admin.twig';
        $this->twig_vars['homes'] = VSKHome::with('photos')
            ->orderBy('position')
            ->orderBy('title')
            ->get();
        $this->render();
    }

    public function showAdminHome($request, $response, $args) {
        $home = VSKHome::with('photos')->find($args['id']);
        $this->view = 'home-admin.twig';
        $this->twig_vars['home'] = $home;
        $this->twig_vars['percentage'] = VSKSettings::where('prop', 'percentage')->first();
        $this->render();
    }

    public function createOrUpdateHome($request, $response, $args)
    {
        set_time_limit(0);

        $params = $request->getParams();
        $params = array_merge($params, [
            'electric' => isset($params['electric']),
            'shower' => isset($params['shower']),
            'bestseller' => isset($params['bestseller'])
        ]);

        $uploader = null;

        if (isset($params['uploader'])) {
            $uploader = $params['uploader'];
            unset($params['uploader']);
        }

        if (isset($args['id'])) {
            $home = VSKHome::find($args['id']);
            $home->update($params);
        } else {
            $home = VSKHome::create($params);
        }

        if ($uploader) {
            $home->checkFiles('photos', $uploader);
        }

        return $response->withHeader('Location', '/admin/vsk/homes');
    }

    public function softDeleteHome($request, $response, $args)
    {
        VSKHome::destroy($args['id']);

        return $response->withHeader('Location', '/admin/vsk/homes');
    }

    public function setPercentage($request, $response, $args)
    {
        $params = $request->getParams();

        if (isset($params['percentage'])) {
            $percentage = $params['percentage'];
            $percentage = $percentage > 100 ? 100 : $percentage < 0 ? 0 : $percentage;

            VSKHome::where('id', '>', 0)->update(['percentage' => $percentage]);
            VSKSettings::updateOrCreate(['prop' => 'percentage'], ['value' => $percentage]);

            $this->ci->flash->addMessage('success', 'Изменения сохранены');
        }

        return $response->withHeader('Location', '/admin/vsk/price');
    }
}
