<?php


namespace ADFM\Controller;


use ADFM\Model\Page;
use ADFM\Model\VSKHome;
use ADFM\Model\VSKDate;
use App\Model\VSKSettings;
use App\Model\VSKTempToken;
use App\Modules\VSK\Builders\PriceBuilder;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Support\Str;

class VSKController extends Controller
{
    private $config;

    public function __construct($container)
    {
        parent::__construct($container);

        $this->config = \Spyc::YAMLLoad(__DIR__ . '/../config.yaml');
    }

    public function showReservation($request, $response, $args)
    {
        $this->twig_vars['page'] = Page::with('image', 'files')->where('alias', 'reservation')->first();
        $this->view = 'reservation.html.twig';
        $this->render();
    }

    public function showMain($request, $response, $args)
    {
        $this->twig_vars['reservation'] = VSKSettings::where('prop', 'reservation')->first();
        $this->twig_vars['homes'] = VSKHome::orderBy('position')->orderBy('title')->get();
        $this->twig_vars['price_list'] = VSKDate::orderBy('from')->with('homes')->get();
        $this->twig_vars['percentage'] = VSKSettings::where('prop', 'percentage')->first();
        $this->view = 'price-admin.twig';
        $this->render();
    }

    public function savePriceList($request, $response, $args)
    {
        $params = $request->getParams();

        foreach ($params as $week) {
            if (isset($week['id'])) {
                $date = VSKDate::find($week['id']);
                $date->update(['from' => $week['from']]);
            } else {
                $date = VSKDate::create([
                    'from' => $week['from']
                ]);
            }

            $homes = [];

            foreach ($week['homes'] as $home) {
                $homes[$home['id']] = [
                    'weekdays_price' => $home['pivot']['weekdays_price'],
                    'weekend_price' => $home['pivot']['weekend_price'],
                    'wholesale' => intval($home['pivot']['wholesale']),
                ];
            }

            $date->homes()->sync($homes);
        }
    }

    public function createOrUpdateDate($request, $response, $args)
    {
        $params = $request->getParams();
        $fields = [
            'from' => $params['from']
        ];

        if (VSKDate::where('from', $params['from'])->get()->count() > 0) {
            $resp = [
                'type' => 'error',
                'message' => "Дата {$params['from']} уже задана"
            ];
        } else {
            if (isset($args['id'])) {
                $resp = VSKDate::whereId($args['id'])->update($fields);
            } else {
                $resp = VSKDate::create($fields);
            }
        }

        return $response->withJson($resp);
    }

    public function disableReservation($request, $response, $args)
    {
        VSKSettings::updateOrCreate(['prop' => 'reservation'], ['value' => 'disabled']);
    }

    public function enableReservation($request, $response, $args)
    {
        VSKSettings::updateOrCreate(['prop' => 'reservation'], ['value' => 'enabled']);
    }

    public function getOrderingUrl($request, $response, $args)
    {
        $params = $request->getBody();
        $params = json_decode($params, true);
        $token = $this->config['token'];
        $vsk_bot_id = $this->config['vsk_bot_id'];
        $chat_ids = $this->config['telegram_chat_id'];
        $chat_id = $params["message"]["chat"]["id"];

        if ($token === $args['token'] && in_array($chat_id, $chat_ids)) {
            $model = VSKTempToken::create([
                'token' => Str::random(64),
                'expires_in' => Carbon::now()->addHour()
            ]);

            $url = $request->getUri()->getBaseUrl() . '/admin/vsk/orders/new?token=' . $model->token;
            $text = "*Ваша ссылка:* [Перейти]({$url})";
            $api = "https://api.telegram.org/bot{$vsk_bot_id}/sendMessage?chat_id={$chat_id}&text=${text}&parse_mode=Markdown";

            file_get_contents($api);
        }
    }
}
