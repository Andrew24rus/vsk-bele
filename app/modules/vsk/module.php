<?php

namespace ADFM\Modules;

use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Capsule\Manager as DB;

class Vsk extends AbstractModules
{
    public $data = [
        'title' => 'ВСК Белё',
        'alias' => 'vsk',
        'description' => '',
        'author' => '',
        'version' => '',
        'last_update' => '',
        'menu_item' => [
            'title' => 'VSK',
            'link' => 'vsk',
            'fa_ico' => 'fa-newspaper-o'
        ]
    ];

    public $tables = [];

    public function run() {
        require_once __DIR__ . '/middleware/ByTempUrl.php';

        //routes
        require_once __DIR__ . '/route/route.php';

        //models
		require_once __DIR__ . '/model/VSKHome.php';
        require_once __DIR__ . '/model/VSKDate.php';
        require_once __DIR__ . '/model/VSKOrder.php';
        require_once __DIR__ . '/model/VSKOrderDate.php';
        require_once __DIR__ . '/model/VSKService.php';
        require_once __DIR__ . '/model/VSKSettings.php';
        require_once __DIR__ . '/model/VSKTempToken.php';
        //controllers
        require_once __DIR__ . '/controller/VSKController.php';
		require_once __DIR__ . '/controller/VSKHomeController.php';
        require_once __DIR__ . '/controller/VSKOrderController.php';
        require_once __DIR__ . '/controller/VSKServiceController.php';

        require_once __DIR__ . '/builders/PriceBuilder.php';
        require_once __DIR__ . '/builders/OrderBuilder.php';
        require_once __DIR__ . '/builders/SberbankBuilder.php';

        //TwigExtension
        require_once __DIR__ . '/TwigExtension/VskExtension.php';
        $this->app->getContainer()['view']->addExtension(new \VskExtension());

        $loader = $this->app->getContainer()['view']->getLoader();
        $loader->addPath(__DIR__ .'/admin/');
        $loader->addPath(__DIR__ .'/themes/');

        \ADFM\Core\Event::addListener('module.getAdminLink', function ($event, &$menu) {
            $menu['modules'][] = [
                'title' => 'Календарь',
                'link' => 'vsk',
                'fa_ico' => 'fa-newspaper-o',
                'submenu' => [
                    [
                        'title' => 'Заказы',
                        'link' => 'vsk/orders'
                    ],
                    [
                        'title' => 'Дома',
                        'link' => 'vsk/homes'
                    ],
                    [
                        'title' => 'Цены',
                        'link' => 'vsk/price'
                    ],
                    [
                        'title' => 'Услуги',
                        'link' => 'vsk/services'
                    ]
                ]
            ];
        });
    }

    public function install() {
        $this->uninstall();

        Capsule::schema()->create('vsk_homes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug')->unique();
            $table->string('title');
            $table->longText('description')->nullable();
            $table->unsignedInteger('square')->nullable();
            $table->unsignedInteger('beds')->nullable();
            $table->boolean('electric')->default(0);
            $table->boolean('shower')->default(0);
            $table->boolean('bestseller')->default(0);
            $table->unsignedInteger('percentage')->default(100);
            $table->integer('position')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Capsule::schema()->create('vsk_dates', function (Blueprint $table) {
            $table->increments('id');
            $table->date('from');
        });

        Capsule::schema()->create('vsk_date_home', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('vsk_home_id');
            $table->unsignedInteger('vsk_date_id');
            $table->unsignedInteger('weekdays_price')->default(3500);
            $table->unsignedInteger('weekend_price')->default(3500);
            $table->tinyInteger('wholesale')->default(0);
            $table->foreign('vsk_home_id')
                ->references('id')
                ->on('vsk_homes')
                ->onDelete('cascade');
            $table->foreign('vsk_date_id')
                ->references('id')
                ->on('vsk_dates')
                ->onDelete('cascade');
        });

        Capsule::schema()->create('vsk_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('vsk_home_id');
            $table->string('sberbank_id')->nullable();
            $table->unsignedDecimal('amount', 12, 2)->default(0);
            $table->enum('status', ['prepare', 'reserved', 'payed', 'canceled', 'completed'])
                ->default('prepare');
            $table->string('name');
            $table->string('phone', 11);
            $table->string('email')->nullable();
            $table->text('note')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Capsule::schema()->create('vsk_order_dates', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('vsk_order_id');
            $table->date('date');
            $table->foreign('vsk_order_id')
                ->references('id')
                ->on('vsk_orders')
                ->onDelete('cascade');
        });

        Capsule::schema()->create('vsk_services', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug');
            $table->string('title');
            $table->text('short_description')->nullable();
            $table->longText('description')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Capsule::schema()->create('vsk_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('prop');
            $table->string('value');
        });

        Capsule::schema()->create('vsk_temp_tokens', function (Blueprint $table) {
            $table->increments('id');
            $table->string('token', 64);
            $table->dateTime('expires_in');
            $table->timestamps();
        });


        $from = Carbon::create(2020, 6, 1, 0, 0, 0);
        $to = Carbon::create(2020, 8, 31, 0, 0, 0);
        $summer = CarbonPeriod::create($from, $to);

        do {
            $current = $summer->current();

            if ($current) {
                $week = $summer->current()->copy()->startOfWeek();
                $summer->skip(7);

                DB::table('vsk_dates')->insert(['from' => $week]);
            }
            else {
                break;
            }
        }
        while ($week->between($from, $to, true));
    }

    public function uninstall () {
        Capsule::schema()->dropIfExists('vsk_date_home');
        Capsule::schema()->dropIfExists('vsk_dates');
        Capsule::schema()->dropIfExists('vsk_homes');
        Capsule::schema()->dropIfExists('vsk_order_dates');
        Capsule::schema()->dropIfExists('vsk_orders');
        Capsule::schema()->dropIfExists('vsk_services');
        Capsule::schema()->dropIfExists('vsk_settings');
        Capsule::schema()->dropIfExists('vsk_temp_tokens');
    }
}
