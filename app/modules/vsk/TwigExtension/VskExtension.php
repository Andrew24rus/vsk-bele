<?php

use ADFM\Controller\VSKOrderController;
use ADFM\Model\VSKHome;
use ADFM\Model\VSKService;
use App\Model\VSKSettings;

class VskExtension extends Twig_Extension
{
    public function getFunctions()
    {
        return
            [
                'getSchedule' => new Twig_Function_Method($this, 'getSchedule'),
                'getHomeOrdersByDate' => new Twig_Function_Method($this,  'getHomeOrdersByDate'),
                'getServices' => new Twig_Function_Method($this,  'getServices', ['needs_environment' => true]),
                'getHomeMinPrice' => new Twig_Function_Method($this,  'getHomeMinPrice', ['needs_environment' => true]),
                'isDisabled' => new Twig_Function_Method($this,  'isDisabled'),
                'getOrderByDate' => new Twig_Function_Method($this,  'getOrderByDate'),
                'prepareThumb' => new Twig_Function_Method($this,  'prepareThumb', ['needs_environment' => true])
            ];
    }

    public function getSchedule()
    {
        $y = date('Y');
        $from = "$y-06-01";
        $to = "$y-08-31";

        return VSKOrderController::getScheduleByDateRange($from, $to);
    }

    public function getHomeOrdersByDate()
    {
        return VSKHome::with(['orders' => function($q) {
            $q->reserved();
        }])
            ->orderBy('position')
            ->orderBy('title')
            ->get();
    }

    public function getServices($twig, $limit)
    {
        $services = VSKService::with('photos');

        if ($limit) {
            $services->limit($limit);
        }

        return $services->get();
    }

    public function getHomeMinPrice($twig, VSKHome $home)
    {
        $min_weekdays_price = $home->priceList->min(function ($q) {
            if ($q->pivot->weekdays_price > 0) {
                return $q->pivot->weekdays_price;
            }
        });

        $min_weekend_price = $home->priceList->min(function ($q) {
            if ($q->pivot->weekend_price > 0) {
                return $q->pivot->weekend_price;
            }
        });

        return min($min_weekdays_price, $min_weekend_price);
    }

    public function isDisabled()
    {
        $reservation = VSKSettings::where('prop', 'reservation')->first();

        return $reservation && $reservation->value === 'disabled';
    }

    public function prepareThumb($twig, $file)
    {
        $file->url = '/imagecache/home_preview' . $file->url;

        return $file;
    }

    public function getOrderByDate(VSKHome $home, $date)
    {
        $order = $home->orders->first(function($q) use($date) {
            return $q->dates->where('date', $date)->count();
        });

        return $order;
    }
}
