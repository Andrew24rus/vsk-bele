<?php

namespace App\Modules\VSK\Builders;

use ADFM\Model\VSKHome;
use ADFM\Model\VSKOrder;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class PriceBuilder
{
    protected $range;

    protected $home;

    protected $currentOrder;

    protected static $period;

    protected static $weekends;

    /**
     * PriceBuilder constructor.
     */
    public function __construct()
    {
        $this->range = [];
        $this->currentOrder = null;

        self::$period = [
            Carbon::today()->startOfDay(),
            Carbon::create(2020, 9, 1, 0, 0, 0)
        ];

        self::$weekends = VSKOrder::$weekendDays;
    }

    public function setCurrentOrder(VSKOrder $order)
    {
        $this->currentOrder = $order;

        return $this;
    }

    /**
     * Устанавливает диапазон дат бронирования
     *
     * @param string $from
     * @param string $to
     * @param bool $check_range
     * @return $this
     * @throws \Exception
     */
    public function setRange($from, $to, $check_range = false)
    {
        $from = self::parse($from);
        $to = self::parse($to);

        if (!$check_range || $this->checkRange($from, $to)) {
            $from = $this->modifyPeriod($from);
            $to = $this->modifyPeriod($to, true);

            $period = CarbonPeriod::create($from, $to);
            $period->excludeEndDate();

            foreach ($period as $date) {
                if (!$check_range || self::checkDate($date)) {
                    $this->range[] = $date;
                }
            }
        }

        return $this;
    }

    /**
     * @param VSKHome $home
     * @return $this
     */
    public function setHome(VSKHome $home)
    {
        $this->home = $home;

        return $this;
    }

    /**
     * @param integer $home_id
     * @return $this
     * @throws ModelNotFoundException
     */
    public function setHomeById($home_id)
    {
        $this->home = VSKHome::findOrFail($home_id);

        return $this;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getWholesaleDates()
    {
        $range = $this->getRange();
        $dates = [];
        $exists = [];

        foreach ($range as $date) {
            if (in_array($date, $exists)) continue;

            if ($this->isWeekend($date)) {
                $priceList = $this->getPriceListByDate($date);

                if ($this->isWholesale($priceList)) {
                    $week = $date->startOfWeek();
                    $from = $week->copy()->modify(self::weekendFirst());
                    $to = $week->copy()->modify(self::weekendLast());
                    $dates[] = [$from, $to];
                    $exists = array_merge($exists, [$from, $to]);
                }
            }
        }

        return $dates;
    }

    public function getPrice()
    {
        $price = 0;
        $range = $this->getRange();

        if ($this->isReserved() === false) {
            $this->loadPriceList();

            foreach ($range as $date) {
                $price_on_date = $this->getPriceByDate($date);

                if (!$price_on_date) {
                    throw new \Exception("{$this->home->title} не доступен для бронирования на {$date->format('d.m.Y')}");
                }

                $price += $price_on_date;
            }
        }

        return $price;
    }

    private function checkRange(Carbon $from, Carbon $to)
    {
        if (self::checkDate($from) && self::checkDate($to)) {
            if ($from < $to) {
                return true;
            }
        }

        throw new \Exception('Указан не верный диапазон дат');
    }

    private static function checkDate(Carbon $date)
    {
        return $date->between(self::$period[0], self::$period[1], true);
    }

    public function getPriceByDate(Carbon $date)
    {
        $priceList = $this->getPriceListByDate($date);

        if ($priceList) {
            if ($this->isWeekend($date) && $this->isWholesale($priceList)) {
                return $this->getWeekendPrice($priceList);
            }

            return $this->getWeekdaysPrice($priceList);
        }

        return null;
    }

    private function getWeekendPrice($priceList)
    {
        return $priceList->pivot->weekend_price;
    }

    private function getWeekdaysPrice($priceList)
    {
        return $priceList->pivot->weekdays_price;
    }

    private function isWeekend(Carbon $date)
    {
        return in_array($date->format('D'), self::$weekends);
    }

    private function isWholesale($priceList) {
        return $priceList->pivot->wholesale === 1;
    }

    /**
     * Проверяет, забронирован ли домик на диапазон дат
     *
     * @return bool
     * @throws \Exception
     */
    private function isReserved()
    {
        $orders = $this->loadOrders();

        if ($this->currentOrder instanceof VSKOrder) {
            $orders = $orders->where('id', '!=', $this->currentOrder->id);
        }

        if ($orders->count() > 0) {
            $dates = $orders->flatMap(function($order) {
                $dates = [];

                foreach ($order->dates as $date) {
                    $date = self::parse($date->date);

                    if (self::checkDate($date)) {
                        $dates[] = $date->format('d.m.Y');
                    }
                }

                return $dates;
            });

            $dates = implode(', ', $dates->toArray());

            throw new \Exception("{$this->getHome()->title} уже забронирован на следующие даты: ${dates}");
        }

        return false;
    }

    /**
     * Загружает заявки для диапазона дат
     *
     * @return Collection
     * @throws \Exception
     */
    private function loadOrders()
    {
        $this->getHome()->load(['orders' => function($q) {
            $q->reserved()
                ->with('dates')
                ->whereHas('dates', function($q) {
                    $q->whereIn('date', $this->getRange());
                });
        }]);

        return $this->home->orders;
    }

    /**
     * Загружает прайс-листы для диапазона дат
     *
     * @return Collection
     * @throws \Exception
     */
    private function loadPriceList()
    {
        $range = array_map(function ($date) {
            return $date->copy()->startOfWeek();
        }, $this->getRange());

        $range = array_unique($range);

        $this->getHome()->load(['priceList' => function($q) use($range) {
            $q->whereIn('from', $range)->orderBy('from');
        }]);

        return $this->home->priceList;
    }

    private function getPriceListByDate(Carbon $date)
    {
        $start_of_week = $date->copy()->startOfWeek()->format('Y-m-d');

        return $this->getHome()
            ->priceList
            ->where('from', $start_of_week)
            ->first();
    }

    /**
     * Получить модель дома
     *
     * @return VSKHome
     * @throws \Exception
     */
    private function getHome()
    {
        if ($this->home instanceof VSKHome) {
            return $this->home;
        }

        throw new \Exception('Параметр $home не задан');
    }

    /**
     * Получить массив дат из диапазона бронирования
     *
     * @return array
     * @throws \Exception
     */
    public function getRange()
    {
        if (is_array($this->range) && count($this->range) > 0) {
            return $this->range;
        }

        throw new \Exception('Не верно указан период проживания');
    }


    /**
     * Сдвигает диапазод дат
     *
     * @param Carbon $date
     * @param bool $end
     * @return Carbon
     */
    private function modifyPeriod(Carbon $date, $end = false)
    {
        if ($this->isWeekend($date)) {
            $priceList = $this->getPriceListByDate($date);

            if ($this->isWholesale($priceList)) {
                $date->startOfWeek()->modify($end ? self::weekendLast() : self::weekendFirst());

                if ($end) {
                    $date->addDay();
                }
            }
        }

        return $date;
    }

    private static function weekendFirst()
    {
        return self::$weekends[0];
    }

    private static function weekendLast()
    {
        return array_slice(self::$weekends, -1)[0];
    }

    /**
     * @param string $date
     * @param string $format
     * @return Carbon
     */
    private static function parse($date, $format = 'Y-m-d')
    {
        return Carbon::createFromFormat($format, $date, 'UTC')->startOfDay();
    }
}
