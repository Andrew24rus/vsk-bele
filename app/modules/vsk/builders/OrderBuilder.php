<?php


namespace App\Modules\VSK\Builders;


use ADFM\Model\VSKHome;
use ADFM\Model\VSKOrder;
use ADFM\Model\VSKOrderDate;
use Carbon\Carbon;

class OrderBuilder
{
    private $order;

    private $home;

    private $sberbankId;

    private $clientName;

    private $clientPhone;

    private $clientEmail;

    /**
     * @var Carbon[]
     */
    private $dates;

    private $note;

    private $status = 'payed_card';

    private $amount;

    public function __construct()
    {
        $this->setDates([]);
        $this->statusPrepare();
    }

    public function setProperty($property, $value)
    {
        $this->order->$property = $value;
        return $this;
    }

    public function setOrder(VSKOrder $order)
    {
        $this->order = $order;

        if (isset($order->home)) {
            $this->setHome($order->home);
        }

        return $this;
    }

    public function setHome(VSKHome $home)
    {
        $this->home = $home;

        return $this;
    }

    public function setHomeById($homeId)
    {
        $this->home = VSKHome::find($homeId);

        return $this;
    }

    public function setSberbankId($sberbankId)
    {
        $this->sberbankId = $sberbankId;

        return $this;
    }

    public function setClientName($clientName)
    {
        $this->clientName = $clientName;

        return $this;
    }

    public function setClientPhone($clientPhone)
    {
        $this->clientPhone = $clientPhone;

        return $this;
    }

    public function setClientEmail($clientEmail)
    {
        $this->clientEmail = $clientEmail;

        return $this;
    }

    /**
     * @param Carbon|Carbon[] $dates
     * @return $this
     */
    public function setDates($dates)
    {
        $this->dates = is_array($dates) ? $dates : [$dates];

        return $this;
    }

    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    public function setAmount($amount)
    {
        $this->amount = abs($amount);

        return $this;
    }

    public function statusPrepare()
    {
        $this->status = 'payed_card';
        $this->order->status = 'payed_card';
        return $this;
    }

    public function statusReserved()
    {
        $this->status = 'reserved';

        return $this;
    }

    public function statusPayed()
    {
        $this->status = 'payed';

        return $this;
    }

    public function statusCompleted()
    {
        $this->status = 'completed';

        return $this;
    }

    public function statusCanceled()
    {
        $this->status = 'canceled';

        return $this;
    }

    public function save()
    {
        $this->order instanceof VSKOrder ? $this->update() : $this->create();

        return $this->order;
    }

    private function create()
    {
        $data = $this->getData();
        $order = VSKOrder::create($data);

        foreach ($this->dates as $date) {
            VSKOrderDate::create([
                'vsk_order_id' => $order->id,
                'date' => $date
            ]);
        }

        $this->setOrder($order);

        return $this->order;
    }

    private function update()
    {
        $data = $this->getData();
        $this->order->update($data);

        $dates = array_map(function(Carbon $date) {
            return $date->format('Y-m-d');
        }, $this->dates);

        $this->order->dates
            ->whereNotIn('date', $dates)
            ->map(function($q) {
                $q->delete();
            });

        foreach ($this->dates as $date) {
            VSKOrderDate::updateOrCreate([
                'vsk_order_id' => $this->order->id,
                'date' => $date
            ]);
        }

        return $this->order;
    }

    private function getData()
    {
        $data = [
            'vsk_home_id' => $this->home->id,
            'sberbank_id' => $this->sberbankId,
            'status' => $this->order->status,
            'amount' => $this->amount,
            'name' => $this->clientName,
            'phone' => $this->clientPhone,
            'email' => $this->clientEmail,
            'note' => $this->note
        ];

        return $data;
    }
}
