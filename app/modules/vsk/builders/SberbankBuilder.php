<?php


namespace App\Modules\VSK\Builders;


class SberbankBuilder
{
    private $url = 'https://3dsec.sberbank.ru/payment/rest';

    private $userName;

    private $password;

    private $orderNumber;

    private $amount;

    private $currency = '643';

    private $returnUrl;

    private $failUrl;

    private $description;

    private $language = 'ru';

    public function __construct()
    {
    }

    /**
     * @param string $username
     * @param string $password
     * @return SberbankBuilder
     */
    public function setCredentials($userName, $password)
    {
        $this->userName = $userName;
        $this->password = $password;

        return $this;
    }

    public function setOrderNumber($orderNumber)
    {
        $this->orderNumber = $orderNumber;

        return $this;
    }

    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }

    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    public function setReturnUrl($returnUrl)
    {
        $this->returnUrl = $returnUrl;

        return $this;
    }

    public function setFailUrl($failUrl)
    {
        $this->failUrl = $failUrl;

        return $this;
    }

    /**
     * Регистрация заказа
     */
    public function register()
    {
        return $this->doRequest('register.do');
    }

    /**
     * @param string $action
     */
    private function doRequest($action)
    {
        $curl = curl_init($this->url . '/' . $action);
        curl_setopt_array($curl, [
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POSTFIELDS => $this->getBody()
        ]);

        $response = curl_exec($curl);

        curl_close($curl);

        $data = json_decode($response);

        return $data;
    }

    private function getBody()
    {
        $body = [
            'userName' => $this->userName,
            'password' => $this->password,
            'orderNumber' => $this->orderNumber,
            'amount' => $this->amount,
            'currency' => $this->currency,
            'description' => $this->description,
            'language' => $this->language,
            'returnUrl' => $this->returnUrl,
            'failUrl' => $this->failUrl
        ];

        return $body;
    }
}
