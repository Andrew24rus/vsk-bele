<?php

use App\Model\VSKTempToken;
use Carbon\Carbon;

class ByTempUrl extends Middleware
{
    public function __invoke($request, $response, $next)
    {
        $params = $request->getParams();

        if (isset($params['token']) && $this->validateToken($params['token'])) {
            return $response = $next($request, $response);
        }

        return $response->withStatus(301)->withHeader('Location', '/auth/login');
    }

    /**
     * @param string $token
     * @return bool
     */
    private function validateToken($token)
    {
        $temp = VSKTempToken::where('token', $token)
            ->where('expires_in', '>', Carbon::now())
            ->first();

        return !!$temp;
    }
}
