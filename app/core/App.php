<?php

use ADFM\Helper\Config;
use App\Helpers\ErrorExceptions;
use Tracy\Debugger;
use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Events\Dispatcher;
use Illuminate\Container\Container;
use Psr7Middlewares\Middleware;
use Illuminate\Database\Capsule\Manager as DB;

define('BASE_PATH', __DIR__ . '/../../');
define('APP_PATH', BASE_PATH . '/app/');
define('CORE_PATH', APP_PATH . '/core/');
define('CONTENT_PATH', BASE_PATH . '/content/');
define('CONFIG_PATH', BASE_PATH . '/config/');
define('ROUTES_PATH', APP_PATH . '/routes/');
define('PLUGINS_PATH', APP_PATH . '/modules/');

class App
{
    const BASE_PATH = BASE_PATH;
    const APP_PATH = APP_PATH;
    const CORE_PATH = CORE_PATH;
    const CONTENT_PATH = CONTENT_PATH;
    const CONFIG_PATH = CONFIG_PATH;
    const ROUTES_PATH = ROUTES_PATH;
    const PLUGINS_PATH = PLUGINS_PATH;

    public function __construct()
    {
        /*Автозагрузчик классов*/
        require_once __DIR__  . '/../../vendor/autoload.php';
        require_once __DIR__ . '/TwigExtensions.php';
        spl_autoload_register(
            function ($class) {
                $classDirs = [
                    self::CORE_PATH,
                    self::BASE_PATH . '/app/model/',
                    self::BASE_PATH . '/app/helpers/',
                    self::BASE_PATH . '/app/interfaces/',
                    self::BASE_PATH . '/app/plugins/',
                    self::BASE_PATH . '/app/controller/',
                    self::BASE_PATH . '/app/core/FileManager/',
                    self::BASE_PATH . '/app/core/middleware/'
                ];
                foreach ($classDirs as $classDir) {
                    if (strripos($class, '\\') == false) {
                        $classFile = $classDir . $class . '.php';
                    } else {
                        $array = explode('\\', $class);
                        $classFile = $classDir . array_pop($array) . '.php';
                    };
                    if (file_exists($classFile)) {
                        include_once $classFile;
                    }
                }
            }
        );

        $config = Config::getInstance();
		setlocale(LC_ALL, $config['main']['locale']);



        /*Инициализация ORM*/
        try{
            $capsule = new Capsule;
            $capsule->addConnection($config['db']);
            $capsule->setEventDispatcher(new Dispatcher(new Container));
            $capsule->setAsGlobal();
            $capsule->bootEloquent();
            DB::connection()->getPdo();
        }
         catch (\Exception $e) {
            echo 'Невозможно подключиться к базе данных. 
                    <br> Возможно нужно проверить фаил /app/config/db.yaml на корректность данных или вы забыли создать базу данных
                    <br> или внести данные вручну <a href="/install.php">через форму</a>';
            ddd($e);
        }


        // Создаем приложение
        $configuration = [
            'settings' => [
                'displayErrorDetails' => true,
            ],
        ];
        $c = new \Slim\Container($configuration);
        $app = new \Slim\App($c);


        // Настраиваем контейнер
        $container = $app->getContainer();
        $container['view'] = function ($container) {
            $view = new \Slim\Views\Twig('../app/themes/vsk/');
            $view->addExtension(new \Slim\Views\TwigExtension(
                $container['router'],
                $container['request']->getUri()
            ));
            $view->addExtension(new TwigExtensions($container));
            $view->addExtension(new TwigDebugger($container));
            $loader = $view->getLoader();
            $loader->addPath('../app/panel/');
            return $view;
        };

        // Not Found Handler
        $container['notFoundHandler'] =
        $container['notAllowedHandler'] = function ($ci) {
            return function ($request, $response) use ($ci) {
                return $ci->view->render($response->withStatus(404), '404.html.twig');
            };
        };

        // Error Handlers
        $container['errorHandler'] =
        $container['phpErrorHandler'] = function ($container) {
            return new ErrorExceptions($container);
        };

        // Add Sentinel
        $container['sentinel'] = function ($container) {
            $sentinel = (new \Cartalyst\Sentinel\Native\Facades\Sentinel())->getSentinel();
            $sentinel->getUserRepository()->setModel(\ADFM\Model\User::class);
            $sentinel->getPersistenceRepository()->setUsersModel(\ADFM\Model\User::class);
            return $sentinel;
        };

        // Флэш сообщения
        $container['flash'] = function () {
            return new \Slim\Flash\Messages();
        };

        // События
        $container['events'] = function () {
            return new League\Event\Emitter;
        };

        $container['config'] = $config['main'];
        //режим разработчика
        if ($config['main']['debug']) {
            Debugger::enable();
        }
        $app->add(new AdminMenu($container));
        $app->add(new Pagination($container));
        $app->add(new GlobalRedirect($container));
        $app->add(Middleware::TrailingSlash(false));



        /*Подключаем плагины*/
//        $emitter = new \ADFM\Core\Event();
//        self::$emitter = $emitter;
        require_once self::PLUGINS_PATH . '/AbstractModules.php';
        $module = new \ADFM\Core\ModuleSystem($app, $config);
        $module->initialize();

        /*Подключаем основные роуты*/
        $routes = scandir(self::ROUTES_PATH);
        unset($routes[0], $routes[1]);
        foreach ($routes as $key => $value) {
            require_once self::ROUTES_PATH . '/' . $value;
        }

        $app->run();
    }
}
